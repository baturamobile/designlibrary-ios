Pod::Spec.new do |s|
  s.name         = "DesignLibrary"
  s.version      = "0.6.0"
  s.summary      = "Design Library for Batura."
  s.description  = <<-DESC
    Design Library for Batura
                   DESC

  s.homepage     = "http://EXAMPLE/DesignLibrary"
  s.license      = "MIT"


  s.author       = { "Rubén Alonso" => "ruben@baturamobile.com" }
  s.platform     = :ios, "9.0"
  s.source       = { :path => '.' }
  s.resources    = ["DesignLibrary/**/*.xcassets", "DesignLibrary/**/*.strings"]

  s.source_files  = "DesignLibrary", "DesignLibrary/**/*.{h,m,swift,xib}"

  s.pod_target_xcconfig = { 'SWIFT_VERSION' => '4' }

end
