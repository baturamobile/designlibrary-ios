# Instalación

## CocoaPods

pod 'DesignLibrary', :git => 'https://baturamobile@bitbucket.org/baturamobile/designlibrary-ios.git'

Con las diferentes opciones que tiene CocoaPods, podemos escoger la version que queramos o la rama de la librería que nos interese:

1. Si queremos coger lo de la rama 'dev':

  pod 'DesignLibrary', :git => 'https://baturamobile@bitbucket.org/baturamobile/designlibrary-ios.git', :branch => 'dev'

2. Si queremos coger lo del tag '0.9.1':

  pod 'DesignLibrary', :git => 'https://baturamobile@bitbucket.org/baturamobile/designlibrary-ios.git', :tag => '0.5.0'



# Components

* [ActionSheetViewController](#markdown-header-actionsheetviewcontroller)
* [ContainerViewController](#markdown-header-containerviewcontroller)
* [PopoverViewController](#markdown-header-popoverviewcontroller)
* [TextInputForm](#markdown-header-textinputform)
* [SelectForm](#markdown-header-selectform)
* [WebController](#markdown-header-webcontroller)
* [Extensions](#markdown-header-extensions)


## ActionSheetViewController
![](resources/actionsheet.png)

```swift
let vc = ActionSheetViewController()
vc.cancelColor = UIColor.blue
vc.spacing = 3
vc.borderColor = UIColor.blue.withAlphaComponent(0.1)
vc.addAction(image: #imageLiteral(resourceName: "doneOrange"), title: "Title", callback: { print("hola") })
vc.addAction(image: #imageLiteral(resourceName: "doneOrange"), title: "Title", callback: { print("hola") })
vc.addAction(image: #imageLiteral(resourceName: "doneOrange"), title: "Title", callback: { print("hola") })
vc.addCancel()

vc.modalPresentationStyle = .overCurrentContext
present(vc, animated: false, completion: nil)
```

### Opciones
* cancelColor
* spacing
* borderColor
* font
* heightItem

## ContainerViewController
![ContainerViewController](resources/container.gif)

```swift
class ViewController: UIViewController {

    var containerVC = ContainerViewController()
    var firstVC: FirstViewController!
    var secondVC: SecondViewController!

    @IBOutlet weak var viewContainer: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        firstVC = FirstViewController()
        secondVC = SecondViewController()
        containerVC.addTo(view: viewContainer, vc: self)
        containerVC.addFirstViewController(vc: firstVC)
    }

    @IBAction func buttonDidTap(_ sender: Any) {
        if containerVC.currentVC == firstVC {
            containerVC.toViewController(vc: secondVC)
        } else {
            containerVC.toViewController(vc: firstVC, animation: .transitionFlipFromLeft)
        }
    }
}
```
### Opciones
* animation: [Apple Documentation](https://developer.apple.com/documentation/uikit/uiviewanimationoptions)


## PopoverViewController
![popover-defult](resources/popover-default.png)

![popover-green](resources/popover-green.png)

```swift
class ViewController: UIViewController {

    @IBOutlet weak var button: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func buttonDidTap(_ sender: UIButton) {
        let popover = PopoverViewController()
        popover.setSource(view: button)
        popover.setSource(rect: button.bounds)
        popover.contentSize = CGSize(width: 200, height: 60)
        popover.backgroundColor = UIColor.green
        popover.text = "Esto es un botón"
        present(popover, animated: true, completion: nil)
    }
}

```
### Metodos
* setSource(view:) Vista que tiene el ancla del popover
* setSource(rect:) Rectangulo en la vista que tiene el ancla

### Opciones
* text: Texto que debe aparecer
* contentSize: Tamaño del recuadro
* backgroundColor: Color de fondo
* font: Tipo de fuente
* fontColor: Color del texto

## TextInputForm

![](resources/textinput1.png)

![](resources/textinput2.png)

![](resources/textinput3.png)

![](resources/textinput4.png)

![](resources/textinput.gif)


```swift
import UIKit
import DesignLibrary

class ViewController: UIViewController {

    @IBOutlet weak var textInput: TextInputForm!
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var label: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        textInput.validate = true
        textInput.validateType = .nif
        textInput.validationDelegate = self
        textInput.errorText = "DNI Inválido"
    }

    @IBAction func buttonDidTap(_ sender: UIButton) {
        view.endEditing(true)
    }
}

extension ViewController: TextInputFormDelegate {
    func didValidate(value: String, valid: Bool) {
        label.text = "\(value) \(valid)"
    }
}
```
### Propiedades, metodos ...
* isValid: Consultar si la validación es correcta
* value: valor que contiene el textfield
* delegate: delegate of textfield
* validateType: tipo de validación
    * email
    * phone
    * cp
    * nif
    * nie
    * empty
    * cif
* keyboardType : tipo de teclado
    * asciiCapable
    * numbersAndPunctuation
    * URL
    * numberPad
    * phonePad
    * namePhonePad
    * emailAddress
    * decimalPad
    * twitter
    * webSearch
    * asciiCapableNumberPad
* textColor
* validate
* secureInput
* labelText
* labelColor
* errorText
* errorColor
* errorHide
* infoText
* infoColor
* image
* placeHolder
* labelFont
* errorLabelFont
* borderOpacity
* borderColor
* showBottomBorder()
* hideBottomBorder()

## SelectForm
![](resources/selectform1.png)

![](resources/selectform2.png)

![](resources/selectform.gif)

```swift
import UIKit
import DesignLibrary

class ViewController: UIViewController {

    @IBOutlet weak var selectForm: SelectForm!

    override func viewDidLoad() {
        super.viewDidLoad()
        selectForm.options = ["1", "2", "3"]
        selectForm.acccesoryImg = #imageLiteral(resourceName: "doneOrange")
        selectForm.parent = self

    }
}

```

### Opciones
#### View
* image:
* imageHide:
* labelText:
* labelHide:
* placeholder:
* placeHolderColor:
* value:
* labelColor:
* errorText:
* errorColor:
* errorHide:
* delegate (SelectFormVCDelegate):
* labelFontText:
* labelFont:
* borderOpacity
* showBottomBorder()
* hideBottomBorder()

#### Lista
* options: Lista de string a mostrar
* parent: ViewController donde se va a mostrar
* acccesoryImg: imagen que se va a mostrar en el elemento seleccionado
* rowHeight: Ancho de las celdas


## WebController

![webcontroller](resources/webcontroller.png)

```swift
import UIKit
import DesignLibrary

class ViewController: UIViewController {

    @IBOutlet weak var button: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func buttonDidTap(_ sender: UIButton) {
        let request = URLRequest(url: URL(string: "https://google.com")!)
        let webController = WebController(request: request)
        present(webController, animated: true, completion: nil)
    }
}
```

## HeaderCollapse
![HeaderCollapse](resources/headercollapse.gif)

#### ScrollView

```swift
import UIKit
import DesignLibrary

class ViewController: UIViewController {

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!

    var headerCollapse : HeaderCollapse!

    override func viewDidLoad() {
        super.viewDidLoad()
        headerCollapse = HeaderCollapse(header: headerView, scrollView: scrollView)
        headerCollapse.minHeight = 50
        headerCollapse.delegate = self
    }
}

extension ViewController: HeaderCollapseDelegate {
    func collapsed(percentage: CGFloat) {
        print(percentage)
    }
}

```

#### WebView

```swift
import UIKit
import DesignLibrary
import WebKit

class ViewController: UIViewController {

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var webView: WKWebView!

    var headerCollapse : HeaderCollapse!

    override func viewDidLoad() {
        super.viewDidLoad()
        headerCollapse = HeaderCollapse(header: headerView, scrollView: webView.scrollView)
        headerCollapse.minHeight = 50
        headerCollapse.delegate = self
    }
}

extension ViewController: HeaderCollapseDelegate {
    func collapsed(percentage: CGFloat) {
        print(percentage)
    }
}

```

#### TableView

```swift
import UIKit
import DesignLibrary

class ViewController: UIViewController {

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var tableView: UITableView!

    var headerCollapse : HeaderCollapse!

    override func viewDidLoad() {
        super.viewDidLoad()
        headerCollapse = HeaderCollapse(header: headerView, scrollView: tableView)
        headerCollapse.minHeight = 50
        headerCollapse.delegate = self
        tableView.delegate = self
    }
}

extension ViewController: HeaderCollapseDelegate {
    func collapsed(percentage: CGFloat) {
        print(percentage)
    }
}

extension ViewController: UITableViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        headerCollpse.scrollViewDidScroll(scrollView)
    }
}

```

### Opciones
- minHeight: Tamaño minimo que tendrá el Header. Por defecto el mismo tamaño que tiene.

### Delegates:
```swift
public protocol HeaderCollapseDelegate: class {
    //Devuelve porcentage del tamaño colapsado
    func collapsed(percentage: CGFloat)
}
```



## Extensions
* UIImage
    * `fixOrientations() -> UIImage`
    * `resizeImage(targetSize: CGSize) -> UIImage`

* UITextField
    * `validate() -> Bool`
    * `validate(equalsTextField: UITextField) -> Bool`
    * `validate(equalsTextField: UITextField, backgroundColor: UIColor) -> Bool`
    * `validate(type: ValidateType, backgroundColor: UIColor) -> Bool`
    * `validate(type: ValidateType, borderColor: CGColor) -> Bool`
    * `validate(type: ValidateType, errorLabel: UILabel, messageError: String, borderColor: CGColor) -> Bool`
    * `validate(type: ValidateType, errorLabel: UILabel, messageError: String) -> Bool`
    * `validate(type: ValidateType) -> Bool`

* String
    * `date(format: String, locale: String? = "es", secondFromGMT: Int? = 0) -> Date?`

* Date
    * `string(format: String, locale: String? = default) -> String`

* Double
    * `string(min: Int? = default, max: Int? = default, separator: String? = default) -> String`
* Float
    * `string(min: Int? = default, max: Int? = default, separator: String? = default) -> String`

* UIView+animation
    * `shakeAnimation(duration: CFTimeInterval? = default, times: Float? = default)`
    * `scaleAnimation(duration: CFTimeInterval? = default, times: Float? = default)`
