//
//  PopoverViewController.swift
//  DesignLibrary
//
//  Created by Rubén Alonso on 26/10/17.
//  Copyright © 2017 Batura Mobile. All rights reserved.
//

import UIKit

public class PopoverViewController: UIViewController {

    @IBOutlet var lblDescription: UILabel!

    private var popover : UIPopoverPresentationController?
    private var attributedString: NSMutableAttributedString?

    public var text: String?
    public var font: UIFont = UIFont.systemFont(ofSize: 17)
    public var fontColor: UIColor = UIColor.black

    public var backgroundColor: UIColor = UIColor.white {
        didSet {
            view.backgroundColor = backgroundColor
            popover?.backgroundColor = backgroundColor
        }
    }

    public var contentSize: CGSize! {
        didSet {
            preferredContentSize = contentSize
        }
    }
    
    public var permittedArrowDirections: UIPopoverArrowDirection = .any {
        didSet {
            popover?.permittedArrowDirections = permittedArrowDirections
        }
    }

    public init() {
        super.init(nibName: String(describing: PopoverViewController.self), bundle: Bundle(for: PopoverViewController.self))
        modalPresentationStyle = .popover
        popover = popoverPresentationController
        popover?.delegate = self
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override public func viewDidLoad() {
        super.viewDidLoad()
        attributedString = NSMutableAttributedString(attributedString: lblDescription.attributedText!)
    }

    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setDescription(text: text!)
    }

    func setDescription(text: String) {
        let range = NSRange(location: 0, length: text.count)
        attributedString?.mutableString.setString(text)
        attributedString?.addAttributes([.font: font,
                                         .foregroundColor: fontColor], range: range)
        lblDescription.attributedText = attributedString
    }

    public func setSource(view: UIView) {
        popover?.sourceView = view
    }

    public func setSource(rect: CGRect) {
        popover?.sourceRect = rect
    }
}

extension PopoverViewController: UIPopoverPresentationControllerDelegate {
    public func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
}

