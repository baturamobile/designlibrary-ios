//
//  HeaderCollapse.swift
//  DesignLibrary
//
//  Created by Rubén Alonso on 27/2/18.
//  Copyright © 2018 Batura Mobile. All rights reserved.
//

import Foundation
import UIKit

public protocol HeaderCollapseDelegate: class {
    func collapsed(percentage: CGFloat)
}

public class HeaderCollapse: NSObject {

    public weak var delegate: HeaderCollapseDelegate?
    public var minHeight: CGFloat!

    private var maxHeight: CGFloat!
    private var header: UIView!
    private var scrollView: UIScrollView!
    private var lastContentOffset: CGFloat = 0

    public override init() {
        super.init()
    }

    public convenience init(header: UIView, scrollView: UIScrollView) {
        self.init()
        self.header = header
        self.scrollView = scrollView
        minHeight = self.header.bounds.size.height
        maxHeight = self.header.bounds.size.height
        setupScrollView()
    }

    private func setupScrollView() {
        self.scrollView.delegate = self
        self.scrollView.contentInset = UIEdgeInsets(top: self.header.bounds.size.height,
                                                    left: 0,
                                                    bottom: 0,
                                                    right: 0)
    }
}

extension HeaderCollapse : UIScrollViewDelegate {
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset.y
        if offset > lastContentOffset {
            if self.header.frame.height > minHeight {
                adaptHeader(offset: offset)
            }
        } else {
            if offset < -minHeight {
                adaptHeader(offset: offset)
            }
        }
        lastContentOffset = offset
        delegate?.collapsed(percentage: calculatePercentage())
    }

    private func adaptHeader(offset: CGFloat) {
        let newHeight = self.header.frame.height + (lastContentOffset - offset)
        self.header.frame = CGRect(x: self.header.frame.origin.x,
                                   y: self.header.frame.origin.y,
                                   width: self.header.frame.size.width,
                                   height: max(minHeight, min(newHeight, maxHeight)))
    }

    private func calculatePercentage() -> CGFloat {
        let currentHeight = self.header.frame.height
        let maxCollapsedHeight = maxHeight - minHeight
        let currentCollapsedHeight = currentHeight - minHeight
        return round((1.0 - (currentCollapsedHeight / maxCollapsedHeight)) * 100) / 100
    }
}
