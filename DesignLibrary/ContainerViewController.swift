//
//  ContainerViewController.swift
//  DesignLibrary
//
//  Created by Rubén Alonso on 12/7/17.
//
//

import UIKit

public class ContainerViewController: UIViewController {

    public var currentVC: UIViewController!
    public var velocity = 0.5

    override public func viewDidLoad() {
        super.viewDidLoad()
    }

    public func addTo(view: UIView, vc: UIViewController) {
        vc.addChildViewController(self)
        view.addSubview(self.view)
        addConstraints(view: view)
        self.didMove(toParentViewController: vc)
    }

    public func addFirstViewController(vc: UIViewController) {
        currentVC = vc
        addChildViewController(vc)
        vc.view.frame = self.view.frame
        view.addSubview(vc.view)
        vc.didMove(toParentViewController: self)
    }

    public func toViewController(vc: UIViewController, animation: UIViewAnimationOptions? = nil) {
        if let animation = animation {
            transitionWithAnimation(vc: vc, animation: animation)
        } else {
            transitionWithoutAnimation(vc: vc)
        }

    }

    private func transitionWithoutAnimation(vc: UIViewController) {
        currentVC.willMove(toParentViewController: nil)
        vc.view.frame = self.view.frame
        view.addSubview(vc.view)

        addChildViewController(vc)
        vc.didMove(toParentViewController: self)

        self.currentVC.view.removeFromSuperview()
        self.currentVC.removeFromParentViewController()
        self.currentVC = vc
    }

    private func transitionWithAnimation(vc: UIViewController, animation: UIViewAnimationOptions) {
        currentVC.willMove(toParentViewController: nil)
        vc.view.frame = self.view.frame
        view.addSubview(vc.view)
        addChildViewController(vc)
        transition(from: currentVC,
                   to: vc,
                   duration: velocity,
                   options: animation,
                   animations: nil) { (_) in
                    vc.didMove(toParentViewController: self)

                    self.currentVC.view.removeFromSuperview()
                    self.currentVC.removeFromParentViewController()
                    self.currentVC = vc
        }
    }

    private func addConstraints(view: UIView) {
        self.view.translatesAutoresizingMaskIntoConstraints = false
        let leading = NSLayoutConstraint(item: self.view,
                                         attribute: .leading,
                                         relatedBy: .equal,
                                         toItem: view,
                                         attribute: .leading,
                                         multiplier: 1,
                                         constant: 0)
        let trailing = NSLayoutConstraint(item: self.view,
                                          attribute: .trailing,
                                          relatedBy: .equal,
                                          toItem: view,
                                          attribute: .trailing,
                                          multiplier: 1,
                                          constant: 0)
        let top = NSLayoutConstraint(item: self.view,
                                     attribute: .top,
                                     relatedBy: .equal,
                                     toItem: view,
                                     attribute: .top,
                                     multiplier: 1,
                                     constant: 0)
        let bottom = NSLayoutConstraint(item: self.view,
                                        attribute: .bottom,
                                        relatedBy: .equal,
                                        toItem: view,
                                        attribute: .bottom,
                                        multiplier: 1,
                                        constant: 0)
        view.addConstraints([leading, trailing, top, bottom])
    }
}
