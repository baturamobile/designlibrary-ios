//
//  GenericCell.swift
//  DesignLibrary
//
//  Created by Rubén Alonso on 18/5/17.
//  Copyright © 2017 Batura Mobile. All rights reserved.
//

import Foundation
import UIKit

public protocol GenericCell: class {
    associatedtype Object: GenericCellModel

    static var reuseId: String {get}
    static var rowHeight: CGFloat {get}
    var item : Object? {get set}

    func configureWith(item: GenericCellModel)
}

extension GenericCell {
    public static func nib() -> UINib {
        return UINib(nibName: String(describing: Self.self), bundle: nil)
    }
    public func configure(item: GenericCellModel) {
        self.item = item as? Object
    }
}
