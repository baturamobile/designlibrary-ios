//
//  TextInputForm.swift
//  DesignLibrary
//
//  Created by Rubén Alonso on 8/5/17.
//  Copyright © 2017 Batura Mobile. All rights reserved.
//

import UIKit

@IBDesignable
open class TextInputForm: UIView {

    @IBOutlet public weak var textField: UITextField!
    @IBOutlet public weak var labelError: UILabel!

    fileprivate var subview: UIView!
    fileprivate var validationType : UITextField.ValidateType = .empty

    var label: UILabel!
    var valid: Bool?
    public var validationDelegate: TextInputFormDelegate?
    fileprivate var errorTxt: String?
    fileprivate var errColor: UIColor?
    fileprivate var infoTxt: String?
    fileprivate var infoClr: UIColor?
    fileprivate var img: UIImage?
    fileprivate var placehold: String?
    fileprivate var opacity: Float = 0.07
    fileprivate var borderCol: UIColor? = .black

    public var labelWidth : CGFloat = 90


    public var isValid: Bool {
        get {
            return valid ?? false
        }
        set {
            valid = newValue
        }
    }

    public var value: String {
        get {
            return textField.text ?? ""
        }
        set {
            textField.text = newValue
        }
    }

    public var delegate: UITextFieldDelegate? {
        get {
            return textField.delegate
        }
        set {
            textField.delegate = newValue
        }
    }

    public var validateType: UITextField.ValidateType {
        get {
            return validationType
        }
        set {
            validationType = newValue
        }
    }

    public var keyboardType: UIKeyboardType {
        get{
            return self.textField.keyboardType
        }
        set {
            self.textField.keyboardType = newValue
        }
    }

    @IBInspectable
    public var textColor: UIColor {
        get{
            return self.textField.textColor!
        }
        set {
            self.textField.textColor = newValue
            if secureInput {
                configureRightView()
            }
        }
    }

    @IBInspectable
    public var validate: Bool = false {
        didSet {
            if validate {
                delegate = self
            } else {
                delegate = nil
            }
        }
    }

    @IBInspectable
    public var secureInput: Bool = false {
        didSet {
            textField?.isSecureTextEntry = secureInput
            if secureInput {
                configureRightView()
            }
        }
    }

    @IBInspectable
    public var labelText: String {
        get {
            return (textField.leftView as? UILabel)?.text ?? ""
        }
        set {
            textField.leftViewMode = .always
            let label = UILabel(frame: CGRect(x: 0, y: 2, width: labelWidth, height: textField.frame.size.height - 2))
            label.text = newValue
            textField.leftView = label
        }
    }

    @IBInspectable
    @objc public dynamic var labelColor : UIColor = UIColor.black {
        didSet {
            (textField.leftView as? UILabel)?.textColor = labelColor
        }
    }


    @IBInspectable
    public var errorText: String {
        get {
            return errorTxt ?? ""
        }
        set {
            errorTxt = newValue
            labelError.text = errorTxt
        }
    }

    @IBInspectable
    public var errorColor : UIColor? {
        get {
            return errColor ?? nil
        }
        set {
            errColor = newValue
            labelError?.textColor = errColor
        }
    }

    @IBInspectable
    public var errorHide: Bool = true {
        didSet {
            labelError?.isHidden = errorHide
        }
    }

    @IBInspectable
    public var infoText: String {
        get {
            return infoTxt ?? ""
        }
        set {
            infoTxt = newValue
        }
    }

    @IBInspectable
    public var infoColor: UIColor? {
        get {
            return infoClr ?? nil
        }
        set {
            infoClr = newValue!
        }
    }

    @IBInspectable
    public var image: UIImage? {
        get {
            return img ?? nil
        }
        set {
            textField.leftViewMode = .always
            let imgView = UIImageView(frame: CGRect(x: 0, y: 8, width: 80, height: textField.frame.size.height - 8))
            imgView.contentMode = .scaleAspectFit
            imgView.image = newValue
            textField.leftView = imgView
            img = newValue
        }
    }

    @IBInspectable
    public var placeHolder: String? {
        get {
            return placehold
        }
        set {
            placehold = newValue
            textField.placeholder = placehold
        }
    }

    public override init(frame: CGRect) {
        super.init(frame: frame)
        loadFromXib()
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadFromXib()
    }

    func loadFromXib() {
        subview = Bundle(for: TextInputForm.self).loadNibNamed(String(describing:TextInputForm.self), owner: self, options: nil)?.first as! UIView
        textField.backgroundColor = self.backgroundColor;
        subview.frame = self.bounds
        addSubview(subview)
        errorHide = true
        showBottomBorder()
    }


    private func configureRightView() {
        let image = UIImage(named: "icView",
                            in: Bundle(for: TextInputForm.self),
                            compatibleWith: nil)?.withRenderingMode(.alwaysTemplate)
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        button.tintColor = textColor
        button.imageView?.contentMode = .scaleAspectFit
        button.setImage(image, for: .normal)
        button.addTarget(self, action: #selector(toggleSecureText), for: .touchUpInside)
        textField.rightView = button
        textField.rightViewMode = .whileEditing
    }

    @objc func toggleSecureText() {
        secureInput = !secureInput
        let button = textField.rightView as! UIButton
        if secureInput {
            let image = UIImage(named: "icView",
                                in: Bundle(for: TextInputForm.self),
                                compatibleWith: nil)?.withRenderingMode(.alwaysTemplate)
            button.tintColor = textColor
            button.setImage(image, for: .normal)
        } else {
            let image = UIImage(named: "icNoview",
                                in: Bundle(for: TextInputForm.self),
                                compatibleWith: nil)?.withRenderingMode(.alwaysTemplate)
            button.tintColor = textColor.withAlphaComponent(0.7)
            button.setImage(image, for: .normal)
        }
    }
}

extension TextInputForm {
    @objc public dynamic var labelFont : UIFont {
        get{
            return textField.font!
        }
        set {
            textField.font = newValue
            (textField.leftView as? UILabel)?.font = newValue
        }
    }

    public var errorLabelFont: UIFont {
        get{
            return labelError.font
        }
        set {
            labelError.font = newValue
        }
    }

    public var borderOpacity: Float {
        get{
            return opacity
        }
        set {
            opacity = newValue
            showBottomBorder()
        }
    }

    public var borderColor: UIColor {
        get{
            return borderCol!
        }
        set {
            borderCol = newValue
            showBottomBorder()
        }
    }

    public func showBottomBorder() {
        self.textField.borderStyle = .none
        //        self.textField.layer.backgroundColor = UIColor.white.cgColor

        self.textField.layer.masksToBounds = false
        self.textField.layer.shadowColor = borderColor.cgColor
        self.textField.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.textField.layer.shadowOpacity = borderOpacity
        self.textField.layer.shadowRadius = 0.0
    }

    public func hideBottomBorder() {
        self.textField.layer.masksToBounds = true
        self.textField.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
    }
}

extension TextInputForm : UITextFieldDelegate {
    public func textFieldDidEndEditing(_ textField: UITextField) {
        labelError.text = ""
        isValid = textField.validate(type: validationType, errorLabel: labelError, messageError: errorText)
        labelError.textColor = !isValid ? errorColor : labelError.textColor
        validationDelegate?.didValidate?(value: value, valid: isValid)
    }

    public func textFieldDidBeginEditing(_ textField: UITextField) {
        if !infoText.isEmpty || infoText.count > 0 {
            labelError.isHidden = false
            labelError.text = infoText
            labelError.textColor = infoColor
        }
    }
}
