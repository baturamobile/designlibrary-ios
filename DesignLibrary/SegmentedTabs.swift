//
//  SegmentedTabs.swift
//  Pods
//
//  Created by Rubén Alonso on 12/7/17.
//
//

import Foundation

extension UISegmentedControl {
    public func customizeAppearance(for height: CGFloat, normal: UIColor, selected: UIColor, border: CGFloat? = nil) {

        setNormalAttribute(attribute: NSAttributedStringKey.foregroundColor.rawValue, value: normal)
        setSelectedAttribute(attribute: NSAttributedStringKey.foregroundColor.rawValue, value: selected)
        setDividerImage(UIImage().colored(with: .clear, size: CGSize(width: 1, height: height)), forLeftSegmentState: .normal, rightSegmentState: .normal, barMetrics: .default)
        setBackgroundImage(UIImage().colored(with: .clear, size: CGSize(width: frame.width, height: height)), for: .normal, barMetrics: .default)
        setBackgroundImage(UIImage().colored(with: .clear, size: CGSize(width: frame.width, height: height)), for: .selected, barMetrics: .default);

        if border != nil {
            for  borderview in subviews {
                let upperBorder: CALayer = CALayer()
                upperBorder.backgroundColor = selected.cgColor
                upperBorder.frame = CGRect(x: 0, y: borderview.frame.size.height - 1, width: borderview.frame.size.width, height: border!)
                borderview.layer.addSublayer(upperBorder)
            }
        }
    }

    public func setFont(font: UIFont) {
        setAttribute(attribute: NSAttributedStringKey.font.rawValue, value: font)
    }

    private func setAttribute(attribute: String, value: Any) {
        setNormalAttribute(attribute: attribute, value: value)
        setSelectedAttribute(attribute: attribute, value: value)
    }

    private func setNormalAttribute(attribute: String, value: Any) {
        var normalAttributes = titleTextAttributes(for: .normal) ?? [AnyHashable: Any]()
        normalAttributes[attribute] = value
        setTitleTextAttributes(normalAttributes, for: .normal)
    }

    private func setSelectedAttribute(attribute: String, value: Any) {
        var selectedAttributes = titleTextAttributes(for: .selected) ?? [AnyHashable: Any]()
        selectedAttributes[attribute] = value
        setTitleTextAttributes(selectedAttributes, for: .selected)
    }
}

extension UIImage {

    func colored(with color: UIColor, size: CGSize) -> UIImage {
        UIGraphicsBeginImageContext(size)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(color.cgColor);
        let rect = CGRect(origin: CGPoint(x: 0, y: 0), size: size)
        context!.fill(rect);
        let image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return image!
    }
}
