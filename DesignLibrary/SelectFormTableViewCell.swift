//
//  SelectFormTableViewCell.swift
//  DesignLibrary
//
//  Created by Rubén Alonso on 30/8/17.
//
//

import UIKit

class SelectFormTableViewCell: UITableViewCell {

    @IBOutlet weak var lblText: UILabel!
    @IBOutlet weak var imgAccessory: UIImageView!

    static let reuseID = String(describing: SelectFormTableViewCell.self)
    public var accessoryImg: UIImage? {
        didSet {
            imgAccessory.image = accessoryImg
        }
    }

    class func nib(image: UIImage? = nil) -> UINib {
        return UINib(nibName: String(describing: SelectFormTableViewCell.self), bundle: Bundle(for: SelectFormTableViewCell.self))
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imgAccessory.isHidden = true
    }

    override func prepareForReuse() {
        imgAccessory.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func showAccesory() {
        imgAccessory.isHidden = accessoryImg != nil ? false : true;
    }
}
