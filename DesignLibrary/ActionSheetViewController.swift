//
//  ActionSheetViewController.swift
//  DesignLibrary
//
//  Created by Rubén Alonso on 3/10/17.
//  Copyright © 2017 Batura Mobile. All rights reserved.
//

import UIKit

public class ActionSheetViewController: UIViewController {

    public var cancelColor = UIColor.red
    public var spacing : CGFloat = 0.0
    public var borderColor: UIColor? = nil
    public var font: UIFont? = nil
    public var heightItem : CGFloat = 44.0

    var stackView = UIStackView()
    var optionsView = UIView()

    var topConstraint: NSLayoutConstraint!
    var bottomConstraint: NSLayoutConstraint!

    override public func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupOptionsView()
        setupStackView()
    }
    
    override public func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        openOptions()
    }

    fileprivate func setupView() {
        view.frame = UIScreen.main.bounds
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissController))
        view.addGestureRecognizer(tap)
    }

    fileprivate func setupOptionsView() {
        view.addSubview(optionsView)
        optionsView.translatesAutoresizingMaskIntoConstraints = false
        optionsView.backgroundColor = UIColor.white
        optionsView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        optionsView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        topConstraint = optionsView.topAnchor.constraint(equalTo: view.bottomAnchor, constant: 0)
        topConstraint.priority = .defaultHigh
        topConstraint.isActive = true
        bottomConstraint = optionsView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0)
        bottomConstraint.priority = .defaultLow
        bottomConstraint.isActive = true
    }

    fileprivate func setupStackView() {
        optionsView.addSubview(stackView)
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        stackView.alignment = .fill
        stackView.spacing = spacing
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.widthAnchor.constraint(equalTo: optionsView.widthAnchor, multiplier: 0.8).isActive = true
        stackView.centerXAnchor.constraint(equalTo: optionsView.centerXAnchor).isActive = true
        stackView.bottomAnchor.constraint(equalTo: optionsView.bottomAnchor, constant: -16).isActive = true
        stackView.topAnchor.constraint(equalTo: optionsView.topAnchor, constant: 16).isActive = true
    }

    func openOptions() {
        bottomConstraint.priority = UILayoutPriority(751)
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
    }

    func closeOption(callback: (()->())?) {
        bottomConstraint.priority = .defaultLow
        UIView.animate(withDuration: 0.2, animations: {
            self.view.layoutIfNeeded()
        }) { (finished) in
            callback?()
        }
    }

    @objc func dismissController() {
        closeOption {
            self.dismiss(animated: false, completion: nil)
        }
    }

    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    public func addCancel () {
        let button = UIButton(type: .custom)
        button.contentHorizontalAlignment = .center
        button.setTitle(NSLocalizedString("BTN_CANCEL", bundle: Bundle(for: ActionSheetViewController.self), comment: ""), for: .normal)
        button.titleLabel?.font = font
        button.setTitleColor(cancelColor, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.heightAnchor.constraint(equalToConstant: heightItem).isActive = true
        button.addTarget(self, action: #selector(dismissController), for: .touchUpInside)
        stackView.addArrangedSubview(button)
    }

    public func addAction(image: UIImage?, title: String, titleColor color: UIColor? = UIColor.black, callback: (() -> ())?){
        let button = ActionButton(type: .custom)
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.heightAnchor.constraint(equalToConstant: heightItem).isActive = true
        button.contentHorizontalAlignment = .left
        button.setImage(image, for: .normal)
        button.setTitle(title, for: .normal)
        button.setTitleColor(color, for: .normal)
        button.titleLabel?.font = font
        button.completion {
            self.closeOption(callback: {
                self.dismiss(animated: false, completion: nil)
                callback?()
            })
        }
        stackView.addArrangedSubview(button)
    }

    public override func viewDidLayoutSubviews() {
        if let bColor = borderColor {
            for button in stackView.arrangedSubviews {
                if button != stackView.arrangedSubviews.last {
                    (button as? ActionButton)?.addBorderBottom(color: bColor)
                }
            }
        }
    }
}
