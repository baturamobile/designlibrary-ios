//
//  DesignLibrary.h
//  DesignLibrary
//
//  Created by Rubén Alonso on 19/5/17.
//  Copyright © 2017 Batura Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for DesignLibrary.
FOUNDATION_EXPORT double DesignLibraryVersionNumber;

//! Project version string for DesignLibrary.
FOUNDATION_EXPORT const unsigned char DesignLibraryVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <DesignLibrary/PublicHeader.h>


