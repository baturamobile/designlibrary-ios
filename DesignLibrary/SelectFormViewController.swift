//
//  SelectFormViewController.swift
//  DesignLibrary
//
//  Created by Rubén Alonso on 22/5/17.
//  Copyright © 2017 Batura Mobile. All rights reserved.
//

import UIKit

protocol SelectFormVCDelegate {
    func valueSelected(value: String)
}

class SelectFormViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    var titleSection: String = ""
    var items: [Any] = [Any]()
    var selected: Int? = nil
    var delegate: SelectFormVCDelegate!
    var font: UIFont!
    var color: UIColor!
    var accessoryImg: UIImage?
    var rowHeight: CGFloat!
    var backImage: UIImage?

    init() {
        super.init(nibName: String(describing: SelectFormViewController.self), bundle: Bundle(for: SelectFormViewController.self))
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        title = titleSection

        tableView.register(SelectFormTableViewCell.nib(), forCellReuseIdentifier: SelectFormTableViewCell.reuseID)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView()
        setupNavigationBar()
    }

    func setupNavigationBar() {
        if let back = backImage {
            let backButton = UIBarButtonItem(image: back,
                                             style: .plain,
                                             target: self,
                                             action: #selector(dismissController))
            navigationItem.leftBarButtonItems = [backButton]
        }
    }

    @objc func dismissController() {
        dismiss(animated: true, completion: nil)
    }
}

extension SelectFormViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SelectFormTableViewCell.reuseID, for: indexPath) as! SelectFormTableViewCell
        cell.accessoryImg = accessoryImg
        cell.lblText?.font = font
        cell.lblText?.textColor = color
        cell.lblText?.text = items[indexPath.row] as? String
        cell.selectionStyle = .none
        if selected == indexPath.row {
            cell.showAccesory()
        }
        return cell
    }
}

extension SelectFormViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.dismiss(animated: true, completion: nil)
        self.delegate.valueSelected(value: items[indexPath.row] as! String)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return rowHeight
    }
}
