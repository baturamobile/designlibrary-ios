//
//  DataSource.swift
//  DesignLibrary
//
//  Created by Rubén Alonso on 18/5/17.
//  Copyright © 2017 Batura Mobile. All rights reserved.
//

import Foundation
import UIKit

public class GenericDataSource<T: GenericCellModel, U: GenericCell>: NSObject, UITableViewDataSource {

    public var items: [T] = [T]()
    var emptyView: UIView?

    public init(items:[T], emptyView: UIView? = nil) {
        self.items = items
        self.emptyView = emptyView
    }

    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if items.count == 0 {
            tableView.isHidden = true
            emptyView?.isHidden = false
        } else {
            tableView.isHidden = false
            emptyView?.isHidden = true
        }
        return items.count
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: U.reuseId, for: indexPath) as! U
        let item = items[indexPath.row] as GenericCellModel
        cell.configureWith(item: item)
        return cell as! UITableViewCell
    }
}
