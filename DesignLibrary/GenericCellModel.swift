//
//  GenericCellModel.swift
//  DesignLibrary
//
//  Created by Rubén Alonso on 18/5/17.
//  Copyright © 2017 Batura Mobile. All rights reserved.
//

import Foundation

@objc public protocol GenericCellModel {}
