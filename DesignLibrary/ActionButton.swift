//
//  ActionButton.swift
//  DesignLibrary
//
//  Created by Rubén Alonso on 19/4/18.
//

import Foundation

public class ActionButton: UIButton {

    var _callback: (() -> ())?

    public func completion(for event: UIControlEvents = .touchUpInside, _ callback: (() -> ())?) {
        _callback = callback
        self.addTarget(self, action: #selector(callFunction), for: event)
    }

    @objc func callFunction() {
        _callback?()
    }

    func addBorderBottom(color: UIColor) {
        let border = CALayer(layer: layer)
        border.frame = CGRect(x: 0, y: self.frame.height - 0.5, width: self.frame.width, height: 0.5)
        border.backgroundColor = color.cgColor
        self.clipsToBounds = true
        self.layer.addSublayer(border)
    }
}
