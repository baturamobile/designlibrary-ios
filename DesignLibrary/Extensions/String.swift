//
//  String.swift
//  DesignLibrary
//
//  Created by Rubén Alonso on 26/2/18.
//  Copyright © 2018 Batura Mobile. All rights reserved.
//

import Foundation

public extension String {
    subscript (i: Int) -> Character {
        return self[index(startIndex, offsetBy: i)]
    }

    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }

    subscript (r: CountableClosedRange<Int>) -> String {
        let start = index(startIndex, offsetBy: r.lowerBound)
        let end = index(startIndex, offsetBy: r.upperBound)
        return String(self[start ..< end])
    }

    func date(format: String, locale: String? = "es", secondFromGMT: Int? = 0) -> Date? {
        if !isEmpty {
            let formatter = DateFormatter()
            formatter.dateFormat = format
            formatter.timeZone = TimeZone(secondsFromGMT: secondFromGMT!)
            formatter.locale = Locale(identifier: locale!)
            return formatter.date(from: self)
        }
        return Date()
    }
}
