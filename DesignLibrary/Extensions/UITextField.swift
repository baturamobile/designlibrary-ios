//
//  UITextField.swift
//  formsvalidation
//
//  Created by Rubén Alonso on 31/1/17.
//  Copyright © 2017 Batura. All rights reserved.
//

import UIKit

public extension UITextField {
    public struct Constants {
        static let CIF1: String = "FJKNPQRSUVW"
        static let CIF2: String = "ABCDEFGHLM"
    }

    public struct Patterns {
        static let EMAIL : String = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
        static let PHONE : String = "^[0-9]{2,3}-? ?[0-9]{6,7}$"
        static let CP: String = "^(0[1-9]|[1-4][0-9]|5[0-2])[0-9]{3}$"
        static let NIF: String = "^[0-9]{8}[TRWAGMYFPDXBNJZSQVHLCKET]{1}$"
        static let EMPTY: String = "^.{1,}$"
        static let NIE: String = "^[XYZ]{1}[0-9]{7}[TRWAGMYFPDXBNJZSQVHLCKET]{1}$"
    }

    public enum ValidateType {
        case email
        case phone
        case cp
        case nif
        case nie
        case empty
        case cif
    }

    public func validate() -> Bool {
        return validate(type: .empty)
    }


    public func validate(equalsTextField: UITextField) -> Bool {
        return (self.text == equalsTextField.text)
    }

    public func validate(equalsTextField: UITextField, backgroundColor: UIColor) -> Bool {
        let isValid = (self.text == equalsTextField.text)
        return self.paintIfValidBackgroundWith(color: backgroundColor, isValid: isValid)
    }

    public func validate(type: ValidateType, backgroundColor:UIColor) -> Bool {
        let isValid = validate(type: type)
        return self.paintIfValidBackgroundWith(color: backgroundColor, isValid: isValid)
    }

    public func validate(type: ValidateType, borderColor: CGColor) -> Bool {
        let isValid = validate(type: type)
        return paintIfValidBorderWith(color: borderColor, isValid: isValid)
    }


    public func validate(type: ValidateType, errorLabel: UILabel, messageError: String, borderColor: CGColor) -> Bool {
        let isValid = validate(type: type, errorLabel: errorLabel, messageError: messageError);
        return paintIfValidBorderWith(color: borderColor, isValid: isValid)
    }

    public func validate(type: ValidateType, errorLabel: UILabel, messageError: String) -> Bool {
        let isValid = validate(type: type)
        if !isValid {
            errorLabel.text = messageError
            errorLabel.isHidden = false
        } else {
            errorLabel.isHidden = true
        }
        return isValid
    }


    public func validate(type: ValidateType) -> Bool {
        let stringLenght = self.text?.count

        let pattern = patternFor(type: type)
        let regex = try! NSRegularExpression(pattern: pattern, options: .anchorsMatchLines)
        if type == .cif {
            return validateCIF()
        }
        let matches = regex.matches(in: self.text!.uppercased(), options: .anchored, range: NSRange(location: 0, length: stringLenght!))
        if matches.count > 0 {
            if(type == .nif || type == .nie){
                return validateNif()
            }
            return true
        }
        return false
    }

    private func paintIfValidBorderWith(color: CGColor, isValid: Bool) -> Bool {
        if !isValid {
            self.layer.borderColor = color
            self.layer.borderWidth = max(self.layer.borderWidth, 1.0);
        }
        return isValid
    }

    private func paintIfValidBackgroundWith(color: UIColor, isValid: Bool) -> Bool{
        if !isValid {
            self.backgroundColor = color
        }
        return isValid
    }

    func validateCIF() -> Bool
    {
        if(text!.count != 9)
        {
            return false
        }

        let valueCif = text![text!.index(after:text!.startIndex) ..< text!.index(before:text!.endIndex)]
        var suma = 0

        var currIndex = valueCif.index(after:valueCif.startIndex)
        while currIndex < valueCif.endIndex {
            if(Int(valueCif[currIndex ..< valueCif.index(after: currIndex)]) != nil)
            {
                suma += Int(valueCif[currIndex ..< valueCif.index(after: currIndex)])!
                currIndex = valueCif.index(currIndex, offsetBy: 2)
            }else
            {
                return false
            }
        }
        
        var suma2 = 0

        var currIndex2 = valueCif.startIndex
        while currIndex2 < valueCif.endIndex {
            let result = Int(valueCif[currIndex2 ..< valueCif.index(after: currIndex2)])! * 2

            if(result > 9)
            {
                suma2 += digitsSum(result)
            }else{
                suma2 += result
            }

            if(currIndex2 != valueCif.index(before:valueCif.endIndex))
            {
                currIndex2 = valueCif.index(currIndex2, offsetBy: 2)
            }else
            {
                break;
            }
        }

        let totalSum = suma + suma2

        let unidadStr = "\(totalSum)"
        var unidadInt = 10 - Int(unidadStr[unidadStr.index(before: unidadStr.endIndex) ..< unidadStr.endIndex])!

        let primerCaracter = text![..<text!.index(after:text!.startIndex)].uppercased()
        let lastchar = String(text![text!.index(before: text!.endIndex)]).uppercased()
        var lastcharchar = lastchar[lastchar.startIndex]
        if Int(lastchar) != nil
        {
            //lastcharchar = String.init(stringInterpolationSegment: UnicodeScalar(64 + Int(lastchar)!))
            lastcharchar = Character.init(UnicodeScalar(64 + Int(lastchar)!)!)
        }

        if(Constants.CIF1.contains(primerCaracter))
        {
            //let value = String.init(stringInterpolationSegment: UnicodeScalar(64+unidadInt))
            let value = Character.init(UnicodeScalar(64+unidadInt)!)
            if(value == lastcharchar)
            {
                return true
            }
        }else if(Constants.CIF2.contains(primerCaracter))
        {
            if(unidadInt == 10)
            {
                unidadInt = 0
            }

            let unidadChar = Character.init(UnicodeScalar(64 + unidadInt)!)
            let lastSubstring = String(text![text!.index(before: text!.endIndex)])
            if("\(unidadInt)" == lastSubstring)
            {
                return true
            }

            if(unidadChar == lastSubstring[lastSubstring.startIndex])
            {
                return true
            }
        }

        return false;
    }

    private func digitsSum(_ value:Int) -> Int {
        var currentResult : Int = 0
        var currentValue = value

        if currentValue <= 100 && currentValue > 10 {
            currentResult += currentValue%10
            currentValue /= 10
            currentResult += currentValue
            return currentResult
        }

        if currentValue == 10 {
            currentResult += 1
            return currentResult
        }
        return currentResult;
    }

    private func validateNif() -> Bool {
        let validChars = "TRWAGMYFPDXBNJZSQVHLCKET"
        let range = text!.startIndex..<text!.index((text?.startIndex)!, offsetBy: 1)
        let textReplaced = text?
            .uppercased()
            .replacingOccurrences(of: "X", with: "0", range: range)
            .replacingOccurrences(of: "Y", with: "1", range: range)
            .replacingOccurrences(of: "Z", with: "2", range: range)

        guard let nif = textReplaced else {
            return false
        }

        let letter = text?.uppercased().last
        let charIndex = Int(nif[0...8])! % 23
        return (validChars[charIndex] == letter)

    }

    private func patternFor(type: ValidateType) -> String {
        var pattern: String
        switch type {
        case .email:
            pattern = Patterns.EMAIL
            break
        case .phone:
            pattern = Patterns.PHONE
            break
        case .cp:
            pattern = Patterns.CP
            break
        case .nif:
            pattern = Patterns.NIF
            break
        case .nie:
            pattern = Patterns.NIE
            break
        default:
            pattern = Patterns.EMPTY
            break
        }
        return pattern
    }

}
