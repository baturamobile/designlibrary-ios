//
//  Double.swift
//  DesignLibrary
//
//  Created by Rubén Alonso on 26/2/18.
//  Copyright © 2018 Batura Mobile. All rights reserved.
//

import Foundation

public extension Double {
    func string(min: Int? = 2, max: Int? = 2, separator: String? = ",") -> String {
        let formatter = NumberFormatter()
        formatter.decimalSeparator = separator
        formatter.maximumFractionDigits = min!
        formatter.minimumFractionDigits = max!
        formatter.minimumIntegerDigits = 1
        return formatter.string(from: NSNumber(value: self))!
    }
}
