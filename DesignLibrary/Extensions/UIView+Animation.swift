//
//  UIView+Animation.swift
//  DesignLibrary
//
//  Created by Rubén Alonso on 26/2/18.
//  Copyright © 2018 Batura Mobile. All rights reserved.
//

import Foundation

public extension UIView {
    func shakeAnimation(duration: CFTimeInterval? = 0.1, times: Float? = 4.0, autoreverse: Bool? = true) {
        let basicAnim = CABasicAnimation(keyPath: "position")
        basicAnim.duration = duration!
        basicAnim.repeatCount = times!
        basicAnim.autoreverses = autoreverse!
        basicAnim.fromValue = NSValue(cgPoint: CGPoint(x: self.frame.midX - 10, y: self.frame.midY))
        basicAnim.toValue = NSValue(cgPoint: CGPoint(x: self.frame.midX + 10, y: self.frame.midY))
        self.layer.add(basicAnim, forKey: "position")
    }

    func scaleAnimation(duration: CFTimeInterval? = 0.1, times: Float? = 1.0, autoreverse: Bool? = true) {
        let basicAnim = CABasicAnimation(keyPath: "transform.scale")
        basicAnim.duration = duration!
        basicAnim.repeatCount = times!
        basicAnim.autoreverses = autoreverse!
        basicAnim.fromValue = 0
        basicAnim.toValue = 1
        self.layer.add(basicAnim, forKey: "scale")
    }
}
