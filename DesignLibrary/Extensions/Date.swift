//
//  Date.swift
//  DesignLibrary
//
//  Created by Rubén Alonso on 26/2/18.
//  Copyright © 2018 Batura Mobile. All rights reserved.
//

import Foundation

public extension Date {
    func string(format: String, locale: String? = "es") -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        formatter.locale = Locale(identifier: locale!)
        return formatter.string(from: self)
    }
}
