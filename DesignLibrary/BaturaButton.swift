//
//  BaturaButton.swift
//  DesignLibrary
//
//  Created by Rubén Alonso on 12/6/18.
//  Copyright © 2018 Batura Mobile. All rights reserved.
//

import Foundation
import UIKit

public class BaturaButton: UIButton {

    public var titleNormal: UIColor = UIColor.black {
        didSet {
            updateStyle(enabled: isEnabled)
        }
    }

    public var titleDisabled: UIColor = UIColor.black {
        didSet {
            updateStyle(enabled: isEnabled)
        }
    }

    public var borderNormal: UIColor = UIColor.clear {
        didSet {
            updateStyle(enabled: isEnabled)
        }
    }

    public var borderDisabled: UIColor = UIColor.clear {
        didSet {
            updateStyle(enabled: isEnabled)
        }
    }

    public var backgroundNormal: UIColor = UIColor.clear {
        didSet {
            updateStyle(enabled: isEnabled)
        }
    }

    public var backgroundDisabled: UIColor = UIColor.clear {
        didSet {
            updateStyle(enabled: isEnabled)
        }
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        updateStyle(enabled: isEnabled)
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        updateStyle(enabled: isEnabled)
    }

    override public var isEnabled: Bool {
        didSet {
            updateStyle(enabled: isEnabled)
        }
    }

    private func updateStyle(enabled:Bool) {
        if enabled {
            setTitleColor(titleNormal, for: .normal)
            backgroundColor = backgroundNormal
            layer.borderColor = borderNormal.cgColor
        } else {
            setTitleColor(titleDisabled, for: .disabled)
            backgroundColor = backgroundDisabled
            layer.borderColor = borderDisabled.cgColor
        }
    }
}
