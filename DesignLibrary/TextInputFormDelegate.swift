//
//  TextInputFormDelegate.swift
//  DesignLibrary
//
//  Created by Rubén Alonso on 11/7/17.
//
//

import Foundation

@objc public protocol TextInputFormDelegate {
    @objc optional func didValidate(value: String, valid: Bool)
}
