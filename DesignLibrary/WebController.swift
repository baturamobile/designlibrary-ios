//
//  WebViewController.swift
//  DesignLibrary
//
//  Created by Rubén Alonso on 30/1/18.
//  Copyright © 2018 Batura Mobile. All rights reserved.
//

import UIKit
import WebKit

public class WebController: UIViewController {

    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var toolbar: UIToolbar!

    var webView: WKWebView!
    var url: String!
    private var request: URLRequest!


    init() {
        super.init(nibName: String(describing:WebController.self), bundle: Bundle(for: WebController.self))
    }

    public convenience init(request: URLRequest) {
        self.init()
        self.request = request
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override public func viewDidLoad() {
        super.viewDidLoad()
        setupWebView()
    }

    func setupWebView() {
        webView = WKWebView()
        webView.navigationDelegate = self
        webView.allowsBackForwardNavigationGestures = true
        webView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(webView)

        NSLayoutConstraint.activate([
            webView.topAnchor.constraint(equalTo: navigationBar.bottomAnchor),
            webView.bottomAnchor.constraint(equalTo: toolbar.topAnchor),
            webView.leftAnchor.constraint(equalTo: view.leftAnchor),
            webView.rightAnchor.constraint(equalTo: view.rightAnchor)])


        if let request = self.request {
            webView.load(request)
        }
    }

    //MARK: - Actions

    @IBAction func btnCloseDidTap(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func btnReloadDidTap(_ sender: Any) {
        webView.reload()
    }

    @IBAction func btnBackDidTap(_ sender: Any) {
        if webView.canGoBack {
            webView.goBack()
        }
    }

    @IBAction func btnForwardDidTap(_ sender: Any) {
        if webView.canGoForward {
            webView.goForward()
        }
    }
}

extension WebController: WKNavigationDelegate {
    public func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        if let url = webView.url {
            navigationBar.topItem?.title = "\(url.host ?? "")"
        }
    }

    public func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        decisionHandler(.allow)
    }
}
