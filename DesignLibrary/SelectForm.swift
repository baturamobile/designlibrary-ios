//
//  SelectForm.swift
//  DesignLibrary
//
//  Created by Rubén Alonso on 22/5/17.
//  Copyright © 2017 Batura Mobile. All rights reserved.
//

import UIKit

public protocol SelectFormDelegate {
    func onValue(value: String)
}


@IBDesignable
open class SelectForm: UIView {

    @IBOutlet fileprivate weak var imgView: UIImageView!
    @IBOutlet public weak var lblText: UILabel!
    @IBOutlet public weak var lblValue: UILabel!
    @IBOutlet fileprivate weak var lblError: UILabel!

    @IBOutlet fileprivate weak var leadConsSuper: NSLayoutConstraint!
    @IBOutlet fileprivate weak var borderBottom: UIView!
    
    fileprivate var errorTxt: String?
    fileprivate var errColor: UIColor?
    fileprivate var subview: UIView!
    fileprivate var hint: String? = ""
    fileprivate var val: String?
    fileprivate var img: UIImage?
    fileprivate var lblColor: UIColor = UIColor.black
    fileprivate var pHolderColor: UIColor = #colorLiteral(red: 0.7803921569, green: 0.7803921569, blue: 0.8039215686, alpha: 1)
    fileprivate var opacity: CGFloat = 0.07


    open var options: [String] = [String]()
    public var parent: UIViewController?
    public var title: String?
    public var delegate: SelectFormDelegate?
    public var acccesoryImg: UIImage?
    public var rowHeight : CGFloat = 54.0
    public var backImage : UIImage?


    @IBInspectable
    public var imageHide: Bool = true {
        didSet {
            imgView.isHidden = imageHide
            leadConsSuper.constant = imgView.bounds.size.width
            if imageHide && labelHide {
                leadConsSuper.constant = 0
            } else {
                leadConsSuper.constant = imgView.bounds.size.width
            }
        }
    }

    @IBInspectable
    public var image: UIImage? {
        get {
            return img ?? nil
        }
        set {
            img = newValue
            imgView.image = img
        }
    }

    @IBInspectable
    public var labelHide: Bool = true {
        didSet {
            lblText.isHidden = labelHide
            leadConsSuper.constant = imgView.bounds.size.width
            if imageHide && labelHide {
                leadConsSuper.constant = 0
            } else {
                leadConsSuper.constant = imgView.bounds.size.width
            }
        }
    }

    @IBInspectable
    public var labelText: String {
        get {
            return lblText.text ?? ""
        }
        set {
            lblText.text = newValue
        }
    }

    @IBInspectable
    public var placeholder: String {
        get {
            return hint ?? ""
        }
        set {
            if value.count == 0 {
                hint = newValue
                lblValue.text = hint
                lblValue.textColor = pHolderColor
            }
        }
    }

    @IBInspectable
    public var placeHolderColor: UIColor {
        get {
            return pHolderColor
        }
        set {
            pHolderColor = newValue
            if value.count == 0 {
                lblValue.text = hint
                lblValue.textColor = pHolderColor
            }
        }
    }

    @IBInspectable
    public var value: String {
        get {
            return val ?? ""
        }
        set {
            val = newValue
            if newValue.count == 0 {
                placeholder = hint!
            } else {
                lblValue.text = val
                lblValue.textColor = lblColor
            }
        }
    }

    @IBInspectable
    @objc public dynamic var labelColor: UIColor {
        get {
            return lblColor
        }
        set {
            lblColor = newValue
            lblValue.textColor = lblColor
        }
    }

    @IBInspectable
    public var errorText: String {
        get {
            return errorTxt ?? ""
        }
        set {
            errorTxt = newValue
            lblError.text = errorTxt
        }
    }

    @IBInspectable
    public var errorColor : UIColor? {
        get {
            return errColor ?? nil
        }
        set {
            errColor = newValue
            lblError?.textColor = errColor
        }
    }

    @IBInspectable
    public var errorHide: Bool = true {
        didSet {
            lblError?.isHidden = errorHide
        }
    }


    public override init(frame: CGRect) {
        super.init(frame: frame)
        loadFromXib()
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadFromXib()
    }

    func loadFromXib() {
        subview = Bundle(for: TextInputForm.self).loadNibNamed(String(describing:SelectForm.self), owner: self, options: nil)?.first as! UIView
        subview.frame = self.bounds
        addSubview(subview)
        errorHide = true
        labelHide = true
        showBottomBorder()
        let gesture = UITapGestureRecognizer(target: self, action: #selector(openOptions))
        self.addGestureRecognizer(gesture)
    }

    @objc func openOptions() {
        let vc = SelectFormViewController()
        let index = value != placeholder ? options.index(of: value) : nil
        vc.items = self.options
        vc.titleSection = title ?? placeholder
        vc.selected = index
        vc.font = labelFont
        vc.color = labelColor
        vc.delegate = self
        vc.accessoryImg = acccesoryImg
        vc.rowHeight = rowHeight
        vc.backImage = backImage
        let nav = UINavigationController(rootViewController: vc)
        parent?.present(nav, animated: true)
    }
}
extension SelectForm: SelectFormVCDelegate {
    func valueSelected(value: String) {
        self.value = value
        self.delegate?.onValue(value: self.value)
    }
}

extension SelectForm {

    @objc public dynamic var labelFontText : UIFont {
        get{
            return lblText.font
        }
        set {
            lblText.font = newValue
        }
    }

    @objc public dynamic var labelFont : UIFont {
        get{
            return lblValue.font
        }
        set {
            lblValue.font = newValue
        }
    }

    public var borderOpacity: CGFloat {
        get{
            return opacity
        }
        set {
            opacity = newValue
        }
    }

    public func showBottomBorder() {
        borderBottom.isHidden = false
        borderBottom.backgroundColor = UIColor.black.withAlphaComponent(borderOpacity)
    }

    public func hideBottomBorder() {
        borderBottom.isHidden = true
    }
}
